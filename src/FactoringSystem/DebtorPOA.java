package FactoringSystem;


/**
* FactoringSystem/DebtorPOA.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from FactoringSystem.idl
* Sunday, December 1, 2019 6:28:33 PM EET
*/

public abstract class DebtorPOA extends org.omg.PortableServer.Servant
 implements FactoringSystem.DebtorOperations, org.omg.CORBA.portable.InvokeHandler
{

  // Constructors

  private static java.util.Hashtable _methods = new java.util.Hashtable ();
  static
  {
    _methods.put ("login", new java.lang.Integer (0));
    _methods.put ("register", new java.lang.Integer (1));
    _methods.put ("book", new java.lang.Integer (2));
    _methods.put ("pay", new java.lang.Integer (3));
    _methods.put ("getOrderList", new java.lang.Integer (4));
    _methods.put ("getProductList", new java.lang.Integer (5));
  }

  public org.omg.CORBA.portable.OutputStream _invoke (String $method,
                                org.omg.CORBA.portable.InputStream in,
                                org.omg.CORBA.portable.ResponseHandler $rh)
  {
    org.omg.CORBA.portable.OutputStream out = null;
    java.lang.Integer __method = (java.lang.Integer)_methods.get ($method);
    if (__method == null)
      throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);

    switch (__method.intValue ())
    {
       case 0:  // FactoringSystem/Debtor/login
       {
         String password = in.read_string ();
         String email = in.read_string ();
         String $result = null;
         $result = this.login (password, email);
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 1:  // FactoringSystem/Debtor/register
       {
         String password = in.read_string ();
         String email = in.read_string ();
         String $result = null;
         $result = this.register (password, email);
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 2:  // FactoringSystem/Debtor/book
       {
         int productID = in.read_long ();
         int quantity = in.read_long ();
         float feeAmount = in.read_float ();
         float prepaidAmount = in.read_float ();
         String $result = null;
         $result = this.book (productID, quantity, feeAmount, prepaidAmount);
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 3:  // FactoringSystem/Debtor/pay
       {
         float amount = in.read_float ();
         String bookID = in.read_string ();
         String $result = null;
         $result = this.pay (amount, bookID);
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 4:  // FactoringSystem/Debtor/getOrderList
       {
         String $result = null;
         $result = this.getOrderList ();
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 5:  // FactoringSystem/Debtor/getProductList
       {
         String $result = null;
         $result = this.getProductList ();
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       default:
         throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);
    }

    return out;
  } // _invoke

  // Type-specific CORBA::Object operations
  private static String[] __ids = {
    "IDL:FactoringSystem/Debtor:1.0"};

  public String[] _all_interfaces (org.omg.PortableServer.POA poa, byte[] objectId)
  {
    return (String[])__ids.clone ();
  }

  public Debtor _this() 
  {
    return DebtorHelper.narrow(
    super._this_object());
  }

  public Debtor _this(org.omg.CORBA.ORB orb) 
  {
    return DebtorHelper.narrow(
    super._this_object(orb));
  }


} // class DebtorPOA
