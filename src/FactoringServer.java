import FactoringSystem.*;
import org.omg.CosNaming.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;
import servants.*;

public class FactoringServer {

    public static void main(String args[]) {
        try{
            // create and initialize the ORB
            ORB orb = ORB.init(args, null);

            // get reference to rootpoa & activate the POAManager
            POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
            rootpoa.the_POAManager().activate();

            // create servant and register it with the ORB
            BankImpl bankImpl = new BankImpl();
            DebtorImpl debtorImpl = new DebtorImpl();

            bankImpl.setORB(orb);
            debtorImpl.setORB(orb);

            // get object reference from the servant
            org.omg.CORBA.Object bankRef = rootpoa.servant_to_reference(bankImpl);
            Bank bankHref = BankHelper.narrow(bankRef);

            org.omg.CORBA.Object debtorRef = rootpoa.servant_to_reference(debtorImpl);
            Debtor debtorHref = DebtorHelper.narrow(debtorRef);

            // get the root naming context
            // NameService invokes the name service
            org.omg.CORBA.Object objRef =
                    orb.resolve_initial_references("NameService");
            // Use NamingContextExt which is part of the Interoperable
            // Naming Service (INS) specification.
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // bind the Object Reference in Naming
            NameComponent bankPath[] = ncRef.to_name( "Bank" );
            ncRef.rebind(bankPath, bankHref);

            NameComponent debtorPath[] = ncRef.to_name( "Debtor" );
            ncRef.rebind(debtorPath, debtorHref);

            System.out.println("FactoringServer ready and waiting ...");

            // wait for invocations from clients
            orb.run();
        }

        catch (Exception e) {
            System.err.println("ERROR: " + e);
            e.printStackTrace(System.out);
        }

        System.out.println("FactoringServer Exiting ...");

    }
}