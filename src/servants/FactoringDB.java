package servants;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.model.ReturnDocument;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.mongodb.client.model.Updates.currentDate;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.inc;

class Response {

    String message;
    Boolean status;

    Response(String message, Boolean status) {
        this.message = message;
        this.status = status;
    }

    public String send() {
        return "Message: \n" + this.message + "\nStatus: " + this.status;
    }
}

public class FactoringDB {

    private static final String DB_NAME = "factoring";
    private static final int postponementTime = 5;

    MongoClient mongoClient;
    MongoDatabase db;
    ObjectId userID;

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }

    private static Date addMinutesToDate(int minutes, Date beforeTime) {
        final long ONE_MINUTE_IN_MILLIS = 60000;

        long curTimeInMs = beforeTime.getTime();
        Date afterAddingMins = new Date(curTimeInMs + (minutes * ONE_MINUTE_IN_MILLIS));
        return afterAddingMins;
    }

    public FactoringDB() {
       try {
           mongoClient = MongoClients.create();
           db = mongoClient.getDatabase("factoring");
       } catch (MongoException e) {
           System.out.println(e.getMessage());
       }
    }

    public void getDBList() {
       MongoIterable<String> list = mongoClient.listDatabaseNames();
        for (String name : list)
            System.out.println(name);
    }

    private List<Document> getProductList() {
        MongoCollection<Document> collection = db.getCollection("product");
        List<Document> products = (List<Document>) collection.find().into(
                new ArrayList<Document>());
        return products;
    }

    private AggregateIterable<Document> getOrderList(Map<String, BasicDBObject> params) {
        MongoCollection<Document> collection = db.getCollection("order");

        DBObject match = new BasicDBObject(params);

        DBObject lookupFields = new BasicDBObject("from", "product");
        lookupFields.put("localField", "product");
        lookupFields.put("foreignField", "_id");
        lookupFields.put("as", "productRef");
        DBObject lookup = new BasicDBObject("$lookup", lookupFields);

        ArrayList conditions = new ArrayList();

        conditions.add(match);
        conditions.add(lookup);

        AggregateIterable<Document> orders = collection.aggregate(conditions);

        return orders;
    }


    private boolean simplePay(float amount, ObjectId bookID) {
        try {
            MongoCollection<Document> collection = db.getCollection("payment");
            Date now = new Date();
            BasicDBObject payment = new BasicDBObject("book", bookID)
                    .append("user", userID)
                    .append("paymentDate", now)
                    .append("amount", amount);
            collection.insertOne(getDocument(payment));
            return true;
        } catch (MongoException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    private static Document getDocument(DBObject doc){
        if(doc == null) return null;
        return new Document(doc.toMap());
    }

    public Response login(String password, String email) {
        try {
            MongoCollection<Document> collection = db.getCollection("user");
            FindIterable<Document> users = collection.find(Filters.eq("email", email));
            MongoCursor<Document> cursor = users.cursor();
            if (cursor.hasNext()) {
                Document doc = cursor.next();
                String docPassword = (String) doc.get("password");
                if (!docPassword.equals(password)) {
                    return new Response("Incorrect password", false);
                }
                userID = (ObjectId) doc.get("_id");
                return new Response("Welcome, " + email, true);
            } else {
                return new Response("User with email '" + email + "' doesn't exist", false);
            }
        } catch (MongoException e) {
            return new Response(e.getMessage(), false);
        }
    }

    public Response register(String password, String email) {
        try {
            MongoCollection<Document> collection = db.getCollection("user");
            FindIterable<Document> users = collection.find(Filters.eq("email", email));
            MongoCursor<Document> cursor = users.cursor();
            if (email.length() == 0 || password.length() == 0)
                return new Response("All fields required!", false);
            if (!validate(email))
                return new Response("Invalid email address", false);
            if (cursor.hasNext()) {
                return new Response("User with email '" + email + "' has already exist", false);
            } else {
                Document user = new Document();
                user.put("email", email);
                user.put("password", password);
                collection.insertOne(user);
                return new Response("You have successfully registered", true);
            }
        } catch (MongoException e) {
            return new Response(e.getMessage(), false);
        }
    }

    public Response product_list() {
        try {
            String message = "";
            List<Document> products = getProductList();
            if (!products.isEmpty()) {
                int index = 0;
                for (Document product : products) {
                    message += index + ": Title: " + product.get("title")
                            + ". Price: " + product.get("price") + "$"
                            + ". Platform: " +  product.get("platform") + "\n";
                    index++;
                }
                return new Response(message, true);
            }
            return new Response("Product list is empty", true);
        } catch (MongoException e) {
            return new Response(e.getMessage(), false);
        }
    }

    public Response order_list() {
        try {
            String message = "";

            Map<String, BasicDBObject> params = new HashMap<String, BasicDBObject>();
            params.put("$match", new BasicDBObject("user", userID));

            AggregateIterable<Document> orders = getOrderList(params);
            MongoCursor<Document> iterator = orders.iterator();
            if (iterator.hasNext()) {
                while(iterator.hasNext()) {
                    Document next = iterator.next();
                    List<Document> products = (List<Document>)next.get("productRef");
                    message += next.get("_id") + " : Debt Amount: " + next.get("debtAmount")
                            + ". Creation Date: " + next.get("creationDate")
                            + ". Deferment Date: " + next.get("defermentDate")
                            + ". Product: " + products.get(0).get("title")
                            + ". Platform: " + products.get(0).get("platform") + "\n";
                }
                return new Response(message, false);
            }
            return new Response("Order list is empty", false);
        } catch (MongoException e) {
            return new Response(e.getMessage(), false);
        }
    }

    public Response book(long productID, long quantity, float feeAmount, float prepaidAmount) {
        try {
            int index = 0;
            Document choosenProduct = null;
            List<Document> products = getProductList();
            for (Document product : products) {
                if (productID == index) {
                    choosenProduct = product;
                    break;
                }
                index++;
            }
            if (choosenProduct != null) {
                MongoCollection<Document> collection = db.getCollection("order");
                float price = Float.parseFloat(String.valueOf(choosenProduct.get("price")));
                float basePrice = price * quantity;
                float debtAmount = basePrice + (basePrice * (feeAmount / 100));
                float firstPayment = debtAmount - debtAmount * (prepaidAmount / 100);

                ObjectId productId = (ObjectId) choosenProduct.get("_id");
                ObjectId customID = new ObjectId();
                Date now = new Date();
                BasicDBObject book = new BasicDBObject("product", productId)
                        .append("_id", customID)
                        .append("user", userID)
                        .append("debtAmount", debtAmount - firstPayment)
                        .append("creationDate", now)
                        .append("defermentDate", addMinutesToDate(postponementTime, now));

                collection.insertOne(getDocument(book));

                boolean transaction = simplePay(firstPayment, customID);

                return new Response("You have successfully ordered an product. Debt: " + (debtAmount - firstPayment), true);
            }
            return new Response("Invalid product id", false);
        } catch(MongoException e) {
            return new Response(e.getMessage(), false);
        }
    }

    public Response pay(float amount, String bookID) {
        try {
            ObjectId id = new ObjectId(bookID);
            boolean transaction = simplePay(amount, id);

            MongoCollection<Document> collection = db.getCollection("order");
            Document updatedDoc = collection.findOneAndUpdate(
                    Filters.eq("_id", id),
                    combine(inc("debtAmount", -amount),
                            currentDate("lastModified")),
                    new FindOneAndUpdateOptions().returnDocument(ReturnDocument.AFTER));

            return new Response("Payment was successful for " + bookID + ", your debt: " + updatedDoc.get("debtAmount"), true);
        } catch (MongoException e) {
            return new Response(e.getMessage(), false);
        }
    }

    public Response _notify() {
        try {
            String message = "You have debts on orders: ";
            Map<String, BasicDBObject> params = new HashMap<String, BasicDBObject>();
            params.put("$match", new BasicDBObject("user", userID));
            params.put("$match", new BasicDBObject("debtAmount", new BasicDBObject("$gt", 0)));

            AggregateIterable<Document> orders = getOrderList(params);
            MongoCursor<Document> iterator = orders.iterator();

            if (iterator.hasNext()) {
                while (iterator.hasNext()) {
                    Document next = iterator.next();
                    message += next.get("_id") + "\n";
                }
                return new Response(message, true);
            }
            return new Response("You have no debts.", false);
        } catch (MongoException e) {
            return new Response(e.getMessage(), false);
        }
    }

    public void close() {
        mongoClient.close();
    }

}
