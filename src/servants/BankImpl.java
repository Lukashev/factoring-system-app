package servants;

import FactoringSystem.BankPOA;
import com.mongodb.client.MongoClient;
import org.omg.CORBA.ORB;
import org.omg.CORBA.Object;

public class BankImpl extends BankPOA {
    private ORB orb;
    private FactoringDB db;

    private static final int COMMISSION_VALUE = 25;
    private static final int PREPAID_VALUE = 90;

    public void setORB(ORB orb_val) {
        orb = orb_val;
        db = new FactoringDB();
    }

    @Override
    public float getCommissionValue() {
        return COMMISSION_VALUE;
    }

    @Override
    public float getPrepaidValue() {
        return PREPAID_VALUE;
    }

    @Override
    public String pay(float amount, String bookID) {
        return null;
    }

    @Override
    public String _notify() {
        Response response = db._notify();
        return response.send();
    }

}