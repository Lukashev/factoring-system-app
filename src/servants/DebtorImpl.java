package servants;

import FactoringSystem.DebtorPOA;
import org.omg.CORBA.ORB;
import org.omg.CORBA.Object;

public class DebtorImpl extends DebtorPOA {

    private ORB orb;
    FactoringDB db;

    public void setORB(ORB orb_val) {
        orb = orb_val;
        db = new FactoringDB();
    }


    @Override
    public String login(String password, String email) {
        Response response = db.login(password, email);
        return response.send();
    }

    @Override
    public String register(String password, String email) {
        Response response = db.register(password, email);
        return response.send();
    }

    @Override
    public String book(int productID, int quantity, float feeAmount, float prepaidAmount) {
        Response response = db.book(productID, quantity, feeAmount, prepaidAmount);
        return response.send();
    }

    @Override
    public String pay(float amount, String bookID) {
        Response response = db.pay(amount, bookID);
        return response.send();
    }

    @Override
    public String getOrderList() {
        Response response = db.order_list();
        return response.send();
    }

    @Override
    public String getProductList() {
        Response response = db.product_list();
        return response.send();
    }

}