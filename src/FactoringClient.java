import FactoringSystem.*;
import org.omg.CosNaming.*;
import org.omg.CORBA.*;

import java.util.Scanner;

public class FactoringClient
{
    static Bank bankImpl;
    static Debtor debtorImpl;
    static UI ui = new UI();

    private static boolean checkAuthStatus(boolean authorized) {
        if (!authorized)
            System.out.println(ui.AUTH_REQUIRED_MESSAGE);
        return authorized;
    }

    public static void main(String args[])
    {
        boolean isAuthorized = false;
        Integer action = null;
        try{
            // create and initialize the ORB
            ORB orb = ORB.init(args, null);

            // get the root naming context
            org.omg.CORBA.Object objRef =
                    orb.resolve_initial_references("NameService");
            // Use NamingContextExt instead of NamingContext. This is
            // part of the Interoperable naming Service.
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // resolve the Object Reference in Naming
            bankImpl = BankHelper.narrow(ncRef.resolve_str("Bank"));
            debtorImpl = DebtorHelper.narrow(ncRef.resolve_str("Debtor"));

            System.out.println("Bank Object Reference : " + bankImpl + "\n");
            System.out.println("Debtor Object Reference : " + debtorImpl);

            do {
                ui.info();
                Scanner in = new Scanner(System.in);
                action = in.nextInt();
                String email = "", password = "";
                int productID, quantity;
                String bookID; float amount = 0;
                switch (action) {
                    case 0:
                        System.exit(0);
                        break;
                    case 1:
                        System.out.println(bankImpl.getCommissionValue() + "%");
                        break;
                    case 2:
                        System.out.println(bankImpl.getPrepaidValue() + "%");
                        break;
                    case 3:
                        Scanner loginInput = new Scanner(System.in);
                        System.out.print("Enter email -> ");
                        email = loginInput.nextLine();

                        System.out.print("Enter password -> ");
                        password = loginInput.nextLine();

                        String message = debtorImpl.login(password,email);
                        if (message.contains("true")) isAuthorized = true;
                        System.out.println(message);

                        break;
                    case 4:
                        Scanner regInput = new Scanner(System.in);

                        System.out.print("Enter email -> ");
                        email = regInput.nextLine();

                        System.out.print("Enter password -> ");
                        password = regInput.nextLine();
                        System.out.println(debtorImpl.register(password,email));
                        break;
                    case 5:
                        if (!checkAuthStatus(isAuthorized)) break;
                        Scanner bookInput = new Scanner(System.in);
                        System.out.println("Choose the product: ");
                        System.out.println(debtorImpl.getProductList());
                        productID = bookInput.nextInt();
                        System.out.print("Enter quantity: ");
                        quantity = bookInput.nextInt();
                        System.out.println(debtorImpl.book(productID,
                                quantity,
                                bankImpl.getCommissionValue(),
                                bankImpl.getPrepaidValue()));
                        break;
                    case 6:
                        if (!checkAuthStatus(isAuthorized)) break;
                        Scanner paymentInput = new Scanner(System.in);
                        String response = debtorImpl.getOrderList();
                        System.out.println("Choose the order id to pay: ");
                        System.out.println(response);
                        bookID = paymentInput.nextLine();
                        System.out.println("Set pay amount: ");
                        amount = paymentInput.nextFloat();
                        System.out.println(debtorImpl.pay(amount, bookID));
                        break;
                    case 7:
                        if (!checkAuthStatus(isAuthorized)) break;
                        System.out.println(debtorImpl.getOrderList());
                        break;
                    case 8:
                        if (!checkAuthStatus(isAuthorized)) break;
                        System.out.println(debtorImpl.getProductList());
                        break;
                    case 9:
                        if (!checkAuthStatus(isAuthorized)) break;
                        System.out.println(bankImpl + " : " + bankImpl._notify());
                        break;
                    default:
                        System.out.println(ui.INVALID_CMD_MESSAGE);
                }
            } while(action != 0);

        } catch (Exception e) {
            System.out.println("ERROR : " + e) ;
            e.printStackTrace(System.out);
        }
    }

}