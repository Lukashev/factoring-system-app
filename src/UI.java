public class UI {
    static final String INVALID_CMD_MESSAGE = "You entered an invalid command...";
    static final String AUTH_REQUIRED_MESSAGE = "You must be logged in to complete this action!";
    enum COMMAND {
        EXIT(0),
        GET_COMMISSION_VALUE(1),
        GET_PREPAID_VALUE(2),
        LOGIN(3),
        REGISTER(4),
        ORDER(5),
        PAY(6),
        GET_ORDER_LIST(7),
        GET_PRODUCT_LIST(8),
        GET_DEBT_REPORT(9);
        private Integer action;
        COMMAND(Integer code){
            this.action = code;
        }
        public Integer getAction(){ return action;}
    }
    COMMAND[] types = COMMAND.values();

    public void info() {
        for (COMMAND s : types) {
            System.out.println(s + " : " + s.getAction());
        }
    }
}
